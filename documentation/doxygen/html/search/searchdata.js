var indexSectionsWithContent =
{
  0: "abcdegilmnprstuv",
  1: "aginpsv",
  2: "abcgilmnt",
  3: "cdegilmprsu",
  4: "a",
  5: "g",
  6: "gpt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Énumérations"
};

