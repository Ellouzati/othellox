#include "board_test.h"

#include <stdio.h>
#include "termUtil.h"
#include "gameState.h"


struct NeighborTest
{
    char posY;
    char posX;
    Neighbor planned[8];
    int toPass;
};

int neighEqual(Neighbor a, Neighbor b)
{
    if(a.posX != b.posX)
        return 0;
    if(a.posY != b.posY)
        return 0;
    if(a.relX != b.relX)
        return 0;
    return a.relY == b.relY;
}

int runNeighborTest(struct NeighborTest test, Neighbor c[8])
{
    int nullPlanned = 8-test.toPass;
    for(int j = 0; j < 8; j++)
    {
        if(neighEqual(c[j], EMPTY_NEIGHBOR))
        {
            nullPlanned--;
            continue;
        }
        for(int k = 0; k < 8; k++)
        {
            if(neighEqual(c[j], test.planned[k]))
            {
                test.planned[k] = EMPTY_NEIGHBOR;
                test.toPass--;
                break;
            }
        }
    }
    if(nullPlanned != 0 || test.toPass != 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int neighborTest()
{
    int passed = 1;
    int testAmount = 9;
    struct NeighborTest tests[testAmount];
    #pragma region : CornerTest
    tests[0] = (struct NeighborTest){
        0, 0,
        { EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            {0, 1, 0, 1}, {1, 1, 1, 1}, {1, 0, 1, 0} },
        3
    };
    tests[1] = (struct NeighborTest){
        7, 7,
        { EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            {7, 6, 0, -1}, {6, 6, -1, -1}, {6, 7, -1, 0} },
        3
    };
    tests[2] = (struct NeighborTest){
        0, 7,
        { EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            {0, 6, 0, -1}, {1, 6, 1, -1}, {1, 7, 1, 0} },
        3
    };
    tests[3] = (struct NeighborTest){
        7, 0,
        { EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            {7, 1, 0, 1}, {6, 1, -1, 1}, {6, 0, -1, 0} },
        3
    };
    #pragma endregion : CornerTest
    #pragma region : SideTest
    tests[4] = (struct NeighborTest){
        3, 0,
        { EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            {3, 1, 0, 1}, {4, 1, 1, 1}, {4, 0, 1, 0}, {2, 0, -1, 0}, {2, 1, -1, 1} },
        5
    };
    tests[5] = (struct NeighborTest){
        0, 3,
        { EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            {1, 3, 1, 0}, {1, 4, 1, 1}, {0, 4, 0, 1}, {0, 2, 0, -1}, {1, 2, 1, -1} },
        5
    };
    tests[6] = (struct NeighborTest){
        4, 7,
        { EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            {4, 6, 0, -1}, {5, 6, 1, -1}, {5, 7, 1, 0}, {3, 7, -1, 0}, {3, 6, -1, -1} },
        5
    };
    tests[7] = (struct NeighborTest){
        7, 4,
        { EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            {7, 5, 0, 1}, {6, 5, -1, 1}, {6, 4, -1, 0}, {7, 3, 0, -1}, {6, 3, -1, -1} },
        5
    };
    #pragma endregion : SideTest 
    #pragma region : MiddleTest
    tests[8] = (struct NeighborTest){
        4, 5,
        { {3, 4, -1, -1}, {3, 5, -1, 0}, {3, 6, -1, 1},
            {4, 4, 0, -1}, {4, 6, 0, 1}, 
            {5, 4, 1, -1}, {5, 5, 1, 0}, {5, 6, 1, 1} },
        8
    };
    #pragma endregion : MiddleTest
    
    Neighbor r_neighbors[8];
    for(int i = 0; i < testAmount; i++)
    {
        printf("\tNeighbor test %d/%d : ...", i+1, testAmount);
        getNeighbors(r_neighbors, tests[i].posY, tests[i].posX);
        
        if(!runNeighborTest(tests[i], r_neighbors))
        {
            CLEAR_LINE();
            printf("\tNeighbor test %d/%d : %s%sFAILED%s%s\n", 
                i+1, testAmount, CSI, RED_COLOR, CSI, WHITE_COLOR);
            passed = -1;
            
        }
        else
        {
            CLEAR_LINE();
        }
    }
    return passed*testAmount;
}

int validNeighborTest()
{
    int passed = 1;
    int vNAmount = 2;
    struct BoardState tests[vNAmount];
    struct NeighborTest actualTests[vNAmount];
    tests[0] = (struct BoardState)
    {
        0,
        {0}, 
        {0}, 
        {0}
    };
    actualTests[0] = (struct NeighborTest)
    {
        2, 3,
        {EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR},
        0
    };

    tests[1] = (struct BoardState)
    {
        4,
        {3,     4,      4,      3   },
        {3,     3,      4,      4   },
        {T_P1,  T_P2,   T_P1,   T_P2}
    };
    actualTests[1] = (struct NeighborTest)
    {
        2, 4,
        {{3, 4, 1, 0}, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR,
            EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR, EMPTY_NEIGHBOR},
        1
    };

    for(int i = 0; i < vNAmount; i++)
    {
        printf("\tValid Neighbor test %d/%d : ...", i+1, vNAmount);
        Neighbor neighbors[8];
        TileType board[8][8];
        for(int j = 0; j < 64; j++)
            board[j/8][j%8] = T_EMPTY;
        for(int j = 0; j < tests[i].tileAmount; j++)
            board[tests[i].posYs[j]][tests[i].posXs[j]] = tests[i].tiles[j];
        getValidNeighbors(board, T_P1, neighbors, actualTests[i].posY, actualTests[i].posX);
        if(!runNeighborTest(actualTests[i], neighbors))
        {
            CLEAR_LINE();
            printf("\tValid Neighbor test %d/%d : %s%sFAILED%s%s\n", 
                i+1, vNAmount, CSI, RED_COLOR, CSI, WHITE_COLOR);
            passed = -1;
            
        }
        else
        {
            CLEAR_LINE();
        }
    }


    return passed*vNAmount;
}

int runBoardTests()
{
    printf("Starting board tests...\n");
    int amt = 0;
    
    if((amt = neighborTest()) < 0)
        return BOARD_FAILED;
    printf("\t%d Neighbor tests : %s%sSUCCESS%s%s\n", amt, CSI, GREEN_COLOR, CSI, WHITE_COLOR);
    if((amt = validNeighborTest()) < 0)
        return BOARD_FAILED;
    printf("\t%d Valid Neighbor tests : %s%sSUCCESS%s%s\n", amt, CSI, GREEN_COLOR, CSI, WHITE_COLOR);
    printf("Board tests passed.\n");

    return 0;
}