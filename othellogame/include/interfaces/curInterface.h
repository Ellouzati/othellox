/**
 * @file curInterface.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Déclare l'interface ncurses
 */

#pragma once
#ifndef NO_CURSES
#include "interface.h"
#include "game.h"

extern struct interface curInterface;

#endif