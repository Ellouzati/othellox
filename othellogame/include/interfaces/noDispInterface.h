/**
 * @file noDispInterface.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Déclare l'interface Sans Affichage
 */
#pragma once

#include "interface.h"
#include "game.h"



extern struct interface noDisplayInterface;