/**
 * @file ai_handler.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Prototypes des fonctions permettant de manipuler les IA
 */
#pragma once
#include "ai_api.h"

/**
 * @brief Structure définissant l'accesseur d'une IA
 */
struct ai_access
{
    void* ai_handle;
    void (*ai_init)(struct ai_inter inter);
    void (*ai_runTurn)(GameState* state, char pId);
    void (*ai_destroy)();
};


/**
 * @brief Crée un accesseur d'IA
 * @param pId Identificateur de l'IA
 * @param ai_name Nom de l'IA
 * @return struct ai_access* Structure accesseur de l'IA
 */
struct ai_access* makeAiAccess(char pId, char* ai_name);

/**
 * @brief Désalloue l'accesseur
 * @param access Structure accesseur de l'IA
 */
void destroyAiAccess(struct ai_access* access);